generate: deps config.pb.c config.pb.h go/config.pb.go third_party/nanopb/nanopb.pb.go
	CGO_ENABLED=0 go generate ./...

deps: ./node_modules/.bin/protoc-gen-ts
./node_modules/.bin/protoc-gen-ts:
	yarn install

