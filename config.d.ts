import * as $protobuf from "protobufjs";
/** Properties of a Config. */
export interface IConfig {

    /** Config brightness */
    brightness?: (number|null);

    /** Config maxBrightness */
    maxBrightness?: (number|null);

    /** Config minBrightness */
    minBrightness?: (number|null);

    /** Config mappings */
    mappings?: (Uint8Array|null);

    /** Config numProfiles */
    numProfiles?: (number|null);

    /** Config profiles */
    profiles?: (Config.IProfile[]|null);

    /** Config switchProfiles */
    switchProfiles?: (Uint8Array|null);

    /** Config controllerType */
    controllerType?: (Config.ControllerType|null);

    /** Config variant */
    variant?: (Config.ControllerVariant|null);
}

/** Represents a Config. */
export class Config implements IConfig {

    /**
     * Constructs a new Config.
     * @param [properties] Properties to set
     */
    constructor(properties?: IConfig);

    /** Config brightness. */
    public brightness: number;

    /** Config maxBrightness. */
    public maxBrightness: number;

    /** Config minBrightness. */
    public minBrightness: number;

    /** Config mappings. */
    public mappings: Uint8Array;

    /** Config numProfiles. */
    public numProfiles: number;

    /** Config profiles. */
    public profiles: Config.IProfile[];

    /** Config switchProfiles. */
    public switchProfiles: Uint8Array;

    /** Config controllerType. */
    public controllerType: Config.ControllerType;

    /** Config variant. */
    public variant: Config.ControllerVariant;

    /**
     * Creates a new Config instance using the specified properties.
     * @param [properties] Properties to set
     * @returns Config instance
     */
    public static create(properties?: IConfig): Config;

    /**
     * Encodes the specified Config message. Does not implicitly {@link Config.verify|verify} messages.
     * @param message Config message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IConfig, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified Config message, length delimited. Does not implicitly {@link Config.verify|verify} messages.
     * @param message Config message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IConfig, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a Config message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns Config
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Config;

    /**
     * Decodes a Config message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns Config
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Config;

    /**
     * Verifies a Config message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a Config message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns Config
     */
    public static fromObject(object: { [k: string]: any }): Config;

    /**
     * Creates a plain object from a Config message. Also converts values to other types if specified.
     * @param message Config
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: Config, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this Config to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

export namespace Config {

    /** ProfileType enum. */
    enum ProfileType {
        STATIC = 0,
        REACTIVE = 1
    }

    /** Properties of a Profile. */
    interface IProfile {

        /** Profile type */
        type?: (Config.ProfileType|null);

        /** Profile breatheInterval */
        breatheInterval?: (number|null);

        /** Profile pstatic */
        pstatic?: (Config.Profile.IStatic|null);

        /** Profile preactive */
        preactive?: (Config.Profile.IReactive|null);

        /** Profile disabledButtons */
        disabledButtons?: (number|null);

        /** Profile animation */
        animation?: (Config.Profile.Animation|null);
    }

    /** Represents a Profile. */
    class Profile implements IProfile {

        /**
         * Constructs a new Profile.
         * @param [properties] Properties to set
         */
        constructor(properties?: Config.IProfile);

        /** Profile type. */
        public type: Config.ProfileType;

        /** Profile breatheInterval. */
        public breatheInterval: number;

        /** Profile pstatic. */
        public pstatic?: (Config.Profile.IStatic|null);

        /** Profile preactive. */
        public preactive?: (Config.Profile.IReactive|null);

        /** Profile disabledButtons. */
        public disabledButtons: number;

        /** Profile animation. */
        public animation: Config.Profile.Animation;

        /** Profile profileSettings. */
        public profileSettings?: ("pstatic"|"preactive");

        /**
         * Creates a new Profile instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Profile instance
         */
        public static create(properties?: Config.IProfile): Config.Profile;

        /**
         * Encodes the specified Profile message. Does not implicitly {@link Config.Profile.verify|verify} messages.
         * @param message Profile message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Config.IProfile, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Profile message, length delimited. Does not implicitly {@link Config.Profile.verify|verify} messages.
         * @param message Profile message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Config.IProfile, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Profile message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Profile
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Config.Profile;

        /**
         * Decodes a Profile message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Profile
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Config.Profile;

        /**
         * Verifies a Profile message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Profile message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Profile
         */
        public static fromObject(object: { [k: string]: any }): Config.Profile;

        /**
         * Creates a plain object from a Profile message. Also converts values to other types if specified.
         * @param message Profile
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Config.Profile, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Profile to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    namespace Profile {

        /** Properties of a Static. */
        interface IStatic {

            /** Static colors */
            colors?: (Uint8Array|null);
        }

        /** Represents a Static. */
        class Static implements IStatic {

            /**
             * Constructs a new Static.
             * @param [properties] Properties to set
             */
            constructor(properties?: Config.Profile.IStatic);

            /** Static colors. */
            public colors: Uint8Array;

            /**
             * Creates a new Static instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Static instance
             */
            public static create(properties?: Config.Profile.IStatic): Config.Profile.Static;

            /**
             * Encodes the specified Static message. Does not implicitly {@link Config.Profile.Static.verify|verify} messages.
             * @param message Static message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: Config.Profile.IStatic, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Static message, length delimited. Does not implicitly {@link Config.Profile.Static.verify|verify} messages.
             * @param message Static message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: Config.Profile.IStatic, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a Static message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Static
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Config.Profile.Static;

            /**
             * Decodes a Static message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Static
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Config.Profile.Static;

            /**
             * Verifies a Static message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a Static message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Static
             */
            public static fromObject(object: { [k: string]: any }): Config.Profile.Static;

            /**
             * Creates a plain object from a Static message. Also converts values to other types if specified.
             * @param message Static
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: Config.Profile.Static, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Static to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a Reactive. */
        interface IReactive {

            /** Reactive colors */
            colors?: (Uint8Array|null);

            /** Reactive reactColors */
            reactColors?: (Uint8Array|null);
        }

        /** Represents a Reactive. */
        class Reactive implements IReactive {

            /**
             * Constructs a new Reactive.
             * @param [properties] Properties to set
             */
            constructor(properties?: Config.Profile.IReactive);

            /** Reactive colors. */
            public colors: Uint8Array;

            /** Reactive reactColors. */
            public reactColors: Uint8Array;

            /**
             * Creates a new Reactive instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Reactive instance
             */
            public static create(properties?: Config.Profile.IReactive): Config.Profile.Reactive;

            /**
             * Encodes the specified Reactive message. Does not implicitly {@link Config.Profile.Reactive.verify|verify} messages.
             * @param message Reactive message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: Config.Profile.IReactive, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Reactive message, length delimited. Does not implicitly {@link Config.Profile.Reactive.verify|verify} messages.
             * @param message Reactive message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: Config.Profile.IReactive, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a Reactive message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Reactive
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Config.Profile.Reactive;

            /**
             * Decodes a Reactive message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Reactive
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Config.Profile.Reactive;

            /**
             * Verifies a Reactive message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a Reactive message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Reactive
             */
            public static fromObject(object: { [k: string]: any }): Config.Profile.Reactive;

            /**
             * Creates a plain object from a Reactive message. Also converts values to other types if specified.
             * @param message Reactive
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: Config.Profile.Reactive, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Reactive to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Animation enum. */
        enum Animation {
            NIL = 0,
            NONE = 1,
            BREATHE = 2
        }
    }

    /** ControllerType enum. */
    enum ControllerType {
        GENERIC = 0,
        SMASH_BOX = 1,
        HIT_BOX = 2,
        CROSSUP = 3,
        BBG_NOSTALGIA = 4
    }

    /** ControllerVariant enum. */
    enum ControllerVariant {
        OTHER = 0,
        FIGHTSTICK = 1,
        STICKLESS = 2,
        FIGHTSTICK_EXD = 3
    }
}
