//go:generate protoc -I../led-spy-protos/third_party/nanopb -I. --nanopb_out=./ --go_out=./go --go_opt=paths=source_relative ./config.proto
//go:generate ./node_modules/.bin/pbjs -t static-module -w commonjs -o config.js ./config.proto
//go:generate ./node_modules/.bin/pbts -o config.d.ts config.js
package main

