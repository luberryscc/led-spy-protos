/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

var $protobuf = require("protobufjs/minimal");

// Common aliases
var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.Config = (function() {

    /**
     * Properties of a Config.
     * @exports IConfig
     * @interface IConfig
     * @property {number|null} [brightness] Config brightness
     * @property {number|null} [maxBrightness] Config maxBrightness
     * @property {number|null} [minBrightness] Config minBrightness
     * @property {Uint8Array|null} [mappings] Config mappings
     * @property {number|null} [numProfiles] Config numProfiles
     * @property {Array.<Config.IProfile>|null} [profiles] Config profiles
     * @property {Uint8Array|null} [switchProfiles] Config switchProfiles
     * @property {Config.ControllerType|null} [controllerType] Config controllerType
     * @property {Config.ControllerVariant|null} [variant] Config variant
     */

    /**
     * Constructs a new Config.
     * @exports Config
     * @classdesc Represents a Config.
     * @implements IConfig
     * @constructor
     * @param {IConfig=} [properties] Properties to set
     */
    function Config(properties) {
        this.profiles = [];
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * Config brightness.
     * @member {number} brightness
     * @memberof Config
     * @instance
     */
    Config.prototype.brightness = 0;

    /**
     * Config maxBrightness.
     * @member {number} maxBrightness
     * @memberof Config
     * @instance
     */
    Config.prototype.maxBrightness = 0;

    /**
     * Config minBrightness.
     * @member {number} minBrightness
     * @memberof Config
     * @instance
     */
    Config.prototype.minBrightness = 0;

    /**
     * Config mappings.
     * @member {Uint8Array} mappings
     * @memberof Config
     * @instance
     */
    Config.prototype.mappings = $util.newBuffer([]);

    /**
     * Config numProfiles.
     * @member {number} numProfiles
     * @memberof Config
     * @instance
     */
    Config.prototype.numProfiles = 0;

    /**
     * Config profiles.
     * @member {Array.<Config.IProfile>} profiles
     * @memberof Config
     * @instance
     */
    Config.prototype.profiles = $util.emptyArray;

    /**
     * Config switchProfiles.
     * @member {Uint8Array} switchProfiles
     * @memberof Config
     * @instance
     */
    Config.prototype.switchProfiles = $util.newBuffer([]);

    /**
     * Config controllerType.
     * @member {Config.ControllerType} controllerType
     * @memberof Config
     * @instance
     */
    Config.prototype.controllerType = 0;

    /**
     * Config variant.
     * @member {Config.ControllerVariant} variant
     * @memberof Config
     * @instance
     */
    Config.prototype.variant = 0;

    /**
     * Creates a new Config instance using the specified properties.
     * @function create
     * @memberof Config
     * @static
     * @param {IConfig=} [properties] Properties to set
     * @returns {Config} Config instance
     */
    Config.create = function create(properties) {
        return new Config(properties);
    };

    /**
     * Encodes the specified Config message. Does not implicitly {@link Config.verify|verify} messages.
     * @function encode
     * @memberof Config
     * @static
     * @param {IConfig} message Config message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Config.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.brightness != null && Object.hasOwnProperty.call(message, "brightness"))
            writer.uint32(/* id 1, wireType 5 =*/13).float(message.brightness);
        if (message.maxBrightness != null && Object.hasOwnProperty.call(message, "maxBrightness"))
            writer.uint32(/* id 2, wireType 5 =*/21).float(message.maxBrightness);
        if (message.minBrightness != null && Object.hasOwnProperty.call(message, "minBrightness"))
            writer.uint32(/* id 3, wireType 5 =*/29).float(message.minBrightness);
        if (message.mappings != null && Object.hasOwnProperty.call(message, "mappings"))
            writer.uint32(/* id 4, wireType 2 =*/34).bytes(message.mappings);
        if (message.numProfiles != null && Object.hasOwnProperty.call(message, "numProfiles"))
            writer.uint32(/* id 5, wireType 0 =*/40).uint32(message.numProfiles);
        if (message.profiles != null && message.profiles.length)
            for (var i = 0; i < message.profiles.length; ++i)
                $root.Config.Profile.encode(message.profiles[i], writer.uint32(/* id 6, wireType 2 =*/50).fork()).ldelim();
        if (message.switchProfiles != null && Object.hasOwnProperty.call(message, "switchProfiles"))
            writer.uint32(/* id 7, wireType 2 =*/58).bytes(message.switchProfiles);
        if (message.controllerType != null && Object.hasOwnProperty.call(message, "controllerType"))
            writer.uint32(/* id 8, wireType 0 =*/64).int32(message.controllerType);
        if (message.variant != null && Object.hasOwnProperty.call(message, "variant"))
            writer.uint32(/* id 9, wireType 0 =*/72).int32(message.variant);
        return writer;
    };

    /**
     * Encodes the specified Config message, length delimited. Does not implicitly {@link Config.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Config
     * @static
     * @param {IConfig} message Config message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Config.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a Config message from the specified reader or buffer.
     * @function decode
     * @memberof Config
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Config} Config
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Config.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.Config();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.brightness = reader.float();
                break;
            case 2:
                message.maxBrightness = reader.float();
                break;
            case 3:
                message.minBrightness = reader.float();
                break;
            case 4:
                message.mappings = reader.bytes();
                break;
            case 5:
                message.numProfiles = reader.uint32();
                break;
            case 6:
                if (!(message.profiles && message.profiles.length))
                    message.profiles = [];
                message.profiles.push($root.Config.Profile.decode(reader, reader.uint32()));
                break;
            case 7:
                message.switchProfiles = reader.bytes();
                break;
            case 8:
                message.controllerType = reader.int32();
                break;
            case 9:
                message.variant = reader.int32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a Config message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Config
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Config} Config
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Config.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a Config message.
     * @function verify
     * @memberof Config
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    Config.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.brightness != null && message.hasOwnProperty("brightness"))
            if (typeof message.brightness !== "number")
                return "brightness: number expected";
        if (message.maxBrightness != null && message.hasOwnProperty("maxBrightness"))
            if (typeof message.maxBrightness !== "number")
                return "maxBrightness: number expected";
        if (message.minBrightness != null && message.hasOwnProperty("minBrightness"))
            if (typeof message.minBrightness !== "number")
                return "minBrightness: number expected";
        if (message.mappings != null && message.hasOwnProperty("mappings"))
            if (!(message.mappings && typeof message.mappings.length === "number" || $util.isString(message.mappings)))
                return "mappings: buffer expected";
        if (message.numProfiles != null && message.hasOwnProperty("numProfiles"))
            if (!$util.isInteger(message.numProfiles))
                return "numProfiles: integer expected";
        if (message.profiles != null && message.hasOwnProperty("profiles")) {
            if (!Array.isArray(message.profiles))
                return "profiles: array expected";
            for (var i = 0; i < message.profiles.length; ++i) {
                var error = $root.Config.Profile.verify(message.profiles[i]);
                if (error)
                    return "profiles." + error;
            }
        }
        if (message.switchProfiles != null && message.hasOwnProperty("switchProfiles"))
            if (!(message.switchProfiles && typeof message.switchProfiles.length === "number" || $util.isString(message.switchProfiles)))
                return "switchProfiles: buffer expected";
        if (message.controllerType != null && message.hasOwnProperty("controllerType"))
            switch (message.controllerType) {
            default:
                return "controllerType: enum value expected";
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
                break;
            }
        if (message.variant != null && message.hasOwnProperty("variant"))
            switch (message.variant) {
            default:
                return "variant: enum value expected";
            case 0:
            case 1:
            case 2:
            case 3:
                break;
            }
        return null;
    };

    /**
     * Creates a Config message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Config
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Config} Config
     */
    Config.fromObject = function fromObject(object) {
        if (object instanceof $root.Config)
            return object;
        var message = new $root.Config();
        if (object.brightness != null)
            message.brightness = Number(object.brightness);
        if (object.maxBrightness != null)
            message.maxBrightness = Number(object.maxBrightness);
        if (object.minBrightness != null)
            message.minBrightness = Number(object.minBrightness);
        if (object.mappings != null)
            if (typeof object.mappings === "string")
                $util.base64.decode(object.mappings, message.mappings = $util.newBuffer($util.base64.length(object.mappings)), 0);
            else if (object.mappings.length)
                message.mappings = object.mappings;
        if (object.numProfiles != null)
            message.numProfiles = object.numProfiles >>> 0;
        if (object.profiles) {
            if (!Array.isArray(object.profiles))
                throw TypeError(".Config.profiles: array expected");
            message.profiles = [];
            for (var i = 0; i < object.profiles.length; ++i) {
                if (typeof object.profiles[i] !== "object")
                    throw TypeError(".Config.profiles: object expected");
                message.profiles[i] = $root.Config.Profile.fromObject(object.profiles[i]);
            }
        }
        if (object.switchProfiles != null)
            if (typeof object.switchProfiles === "string")
                $util.base64.decode(object.switchProfiles, message.switchProfiles = $util.newBuffer($util.base64.length(object.switchProfiles)), 0);
            else if (object.switchProfiles.length)
                message.switchProfiles = object.switchProfiles;
        switch (object.controllerType) {
        case "GENERIC":
        case 0:
            message.controllerType = 0;
            break;
        case "SMASH_BOX":
        case 1:
            message.controllerType = 1;
            break;
        case "HIT_BOX":
        case 2:
            message.controllerType = 2;
            break;
        case "CROSSUP":
        case 3:
            message.controllerType = 3;
            break;
        case "BBG_NOSTALGIA":
        case 4:
            message.controllerType = 4;
            break;
        }
        switch (object.variant) {
        case "OTHER":
        case 0:
            message.variant = 0;
            break;
        case "FIGHTSTICK":
        case 1:
            message.variant = 1;
            break;
        case "STICKLESS":
        case 2:
            message.variant = 2;
            break;
        case "FIGHTSTICK_EXD":
        case 3:
            message.variant = 3;
            break;
        }
        return message;
    };

    /**
     * Creates a plain object from a Config message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Config
     * @static
     * @param {Config} message Config
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Config.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.arrays || options.defaults)
            object.profiles = [];
        if (options.defaults) {
            object.brightness = 0;
            object.maxBrightness = 0;
            object.minBrightness = 0;
            if (options.bytes === String)
                object.mappings = "";
            else {
                object.mappings = [];
                if (options.bytes !== Array)
                    object.mappings = $util.newBuffer(object.mappings);
            }
            object.numProfiles = 0;
            if (options.bytes === String)
                object.switchProfiles = "";
            else {
                object.switchProfiles = [];
                if (options.bytes !== Array)
                    object.switchProfiles = $util.newBuffer(object.switchProfiles);
            }
            object.controllerType = options.enums === String ? "GENERIC" : 0;
            object.variant = options.enums === String ? "OTHER" : 0;
        }
        if (message.brightness != null && message.hasOwnProperty("brightness"))
            object.brightness = options.json && !isFinite(message.brightness) ? String(message.brightness) : message.brightness;
        if (message.maxBrightness != null && message.hasOwnProperty("maxBrightness"))
            object.maxBrightness = options.json && !isFinite(message.maxBrightness) ? String(message.maxBrightness) : message.maxBrightness;
        if (message.minBrightness != null && message.hasOwnProperty("minBrightness"))
            object.minBrightness = options.json && !isFinite(message.minBrightness) ? String(message.minBrightness) : message.minBrightness;
        if (message.mappings != null && message.hasOwnProperty("mappings"))
            object.mappings = options.bytes === String ? $util.base64.encode(message.mappings, 0, message.mappings.length) : options.bytes === Array ? Array.prototype.slice.call(message.mappings) : message.mappings;
        if (message.numProfiles != null && message.hasOwnProperty("numProfiles"))
            object.numProfiles = message.numProfiles;
        if (message.profiles && message.profiles.length) {
            object.profiles = [];
            for (var j = 0; j < message.profiles.length; ++j)
                object.profiles[j] = $root.Config.Profile.toObject(message.profiles[j], options);
        }
        if (message.switchProfiles != null && message.hasOwnProperty("switchProfiles"))
            object.switchProfiles = options.bytes === String ? $util.base64.encode(message.switchProfiles, 0, message.switchProfiles.length) : options.bytes === Array ? Array.prototype.slice.call(message.switchProfiles) : message.switchProfiles;
        if (message.controllerType != null && message.hasOwnProperty("controllerType"))
            object.controllerType = options.enums === String ? $root.Config.ControllerType[message.controllerType] : message.controllerType;
        if (message.variant != null && message.hasOwnProperty("variant"))
            object.variant = options.enums === String ? $root.Config.ControllerVariant[message.variant] : message.variant;
        return object;
    };

    /**
     * Converts this Config to JSON.
     * @function toJSON
     * @memberof Config
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    Config.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    /**
     * ProfileType enum.
     * @name Config.ProfileType
     * @enum {number}
     * @property {number} STATIC=0 STATIC value
     * @property {number} REACTIVE=1 REACTIVE value
     */
    Config.ProfileType = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "STATIC"] = 0;
        values[valuesById[1] = "REACTIVE"] = 1;
        return values;
    })();

    Config.Profile = (function() {

        /**
         * Properties of a Profile.
         * @memberof Config
         * @interface IProfile
         * @property {Config.ProfileType|null} [type] Profile type
         * @property {number|null} [breatheInterval] Profile breatheInterval
         * @property {Config.Profile.IStatic|null} [pstatic] Profile pstatic
         * @property {Config.Profile.IReactive|null} [preactive] Profile preactive
         * @property {number|null} [disabledButtons] Profile disabledButtons
         * @property {Config.Profile.Animation|null} [animation] Profile animation
         */

        /**
         * Constructs a new Profile.
         * @memberof Config
         * @classdesc Represents a Profile.
         * @implements IProfile
         * @constructor
         * @param {Config.IProfile=} [properties] Properties to set
         */
        function Profile(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Profile type.
         * @member {Config.ProfileType} type
         * @memberof Config.Profile
         * @instance
         */
        Profile.prototype.type = 0;

        /**
         * Profile breatheInterval.
         * @member {number} breatheInterval
         * @memberof Config.Profile
         * @instance
         */
        Profile.prototype.breatheInterval = 0;

        /**
         * Profile pstatic.
         * @member {Config.Profile.IStatic|null|undefined} pstatic
         * @memberof Config.Profile
         * @instance
         */
        Profile.prototype.pstatic = null;

        /**
         * Profile preactive.
         * @member {Config.Profile.IReactive|null|undefined} preactive
         * @memberof Config.Profile
         * @instance
         */
        Profile.prototype.preactive = null;

        /**
         * Profile disabledButtons.
         * @member {number} disabledButtons
         * @memberof Config.Profile
         * @instance
         */
        Profile.prototype.disabledButtons = 0;

        /**
         * Profile animation.
         * @member {Config.Profile.Animation} animation
         * @memberof Config.Profile
         * @instance
         */
        Profile.prototype.animation = 0;

        // OneOf field names bound to virtual getters and setters
        var $oneOfFields;

        /**
         * Profile profileSettings.
         * @member {"pstatic"|"preactive"|undefined} profileSettings
         * @memberof Config.Profile
         * @instance
         */
        Object.defineProperty(Profile.prototype, "profileSettings", {
            get: $util.oneOfGetter($oneOfFields = ["pstatic", "preactive"]),
            set: $util.oneOfSetter($oneOfFields)
        });

        /**
         * Creates a new Profile instance using the specified properties.
         * @function create
         * @memberof Config.Profile
         * @static
         * @param {Config.IProfile=} [properties] Properties to set
         * @returns {Config.Profile} Profile instance
         */
        Profile.create = function create(properties) {
            return new Profile(properties);
        };

        /**
         * Encodes the specified Profile message. Does not implicitly {@link Config.Profile.verify|verify} messages.
         * @function encode
         * @memberof Config.Profile
         * @static
         * @param {Config.IProfile} message Profile message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Profile.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.type != null && Object.hasOwnProperty.call(message, "type"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.type);
            if (message.breatheInterval != null && Object.hasOwnProperty.call(message, "breatheInterval"))
                writer.uint32(/* id 2, wireType 5 =*/21).float(message.breatheInterval);
            if (message.pstatic != null && Object.hasOwnProperty.call(message, "pstatic"))
                $root.Config.Profile.Static.encode(message.pstatic, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            if (message.preactive != null && Object.hasOwnProperty.call(message, "preactive"))
                $root.Config.Profile.Reactive.encode(message.preactive, writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
            if (message.disabledButtons != null && Object.hasOwnProperty.call(message, "disabledButtons"))
                writer.uint32(/* id 5, wireType 5 =*/45).fixed32(message.disabledButtons);
            if (message.animation != null && Object.hasOwnProperty.call(message, "animation"))
                writer.uint32(/* id 6, wireType 0 =*/48).int32(message.animation);
            return writer;
        };

        /**
         * Encodes the specified Profile message, length delimited. Does not implicitly {@link Config.Profile.verify|verify} messages.
         * @function encodeDelimited
         * @memberof Config.Profile
         * @static
         * @param {Config.IProfile} message Profile message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Profile.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Profile message from the specified reader or buffer.
         * @function decode
         * @memberof Config.Profile
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {Config.Profile} Profile
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Profile.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.Config.Profile();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.type = reader.int32();
                    break;
                case 2:
                    message.breatheInterval = reader.float();
                    break;
                case 3:
                    message.pstatic = $root.Config.Profile.Static.decode(reader, reader.uint32());
                    break;
                case 4:
                    message.preactive = $root.Config.Profile.Reactive.decode(reader, reader.uint32());
                    break;
                case 5:
                    message.disabledButtons = reader.fixed32();
                    break;
                case 6:
                    message.animation = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Profile message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof Config.Profile
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {Config.Profile} Profile
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Profile.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Profile message.
         * @function verify
         * @memberof Config.Profile
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Profile.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            var properties = {};
            if (message.type != null && message.hasOwnProperty("type"))
                switch (message.type) {
                default:
                    return "type: enum value expected";
                case 0:
                case 1:
                    break;
                }
            if (message.breatheInterval != null && message.hasOwnProperty("breatheInterval"))
                if (typeof message.breatheInterval !== "number")
                    return "breatheInterval: number expected";
            if (message.pstatic != null && message.hasOwnProperty("pstatic")) {
                properties.profileSettings = 1;
                {
                    var error = $root.Config.Profile.Static.verify(message.pstatic);
                    if (error)
                        return "pstatic." + error;
                }
            }
            if (message.preactive != null && message.hasOwnProperty("preactive")) {
                if (properties.profileSettings === 1)
                    return "profileSettings: multiple values";
                properties.profileSettings = 1;
                {
                    var error = $root.Config.Profile.Reactive.verify(message.preactive);
                    if (error)
                        return "preactive." + error;
                }
            }
            if (message.disabledButtons != null && message.hasOwnProperty("disabledButtons"))
                if (!$util.isInteger(message.disabledButtons))
                    return "disabledButtons: integer expected";
            if (message.animation != null && message.hasOwnProperty("animation"))
                switch (message.animation) {
                default:
                    return "animation: enum value expected";
                case 0:
                case 1:
                case 2:
                    break;
                }
            return null;
        };

        /**
         * Creates a Profile message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof Config.Profile
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {Config.Profile} Profile
         */
        Profile.fromObject = function fromObject(object) {
            if (object instanceof $root.Config.Profile)
                return object;
            var message = new $root.Config.Profile();
            switch (object.type) {
            case "STATIC":
            case 0:
                message.type = 0;
                break;
            case "REACTIVE":
            case 1:
                message.type = 1;
                break;
            }
            if (object.breatheInterval != null)
                message.breatheInterval = Number(object.breatheInterval);
            if (object.pstatic != null) {
                if (typeof object.pstatic !== "object")
                    throw TypeError(".Config.Profile.pstatic: object expected");
                message.pstatic = $root.Config.Profile.Static.fromObject(object.pstatic);
            }
            if (object.preactive != null) {
                if (typeof object.preactive !== "object")
                    throw TypeError(".Config.Profile.preactive: object expected");
                message.preactive = $root.Config.Profile.Reactive.fromObject(object.preactive);
            }
            if (object.disabledButtons != null)
                message.disabledButtons = object.disabledButtons >>> 0;
            switch (object.animation) {
            case "NIL":
            case 0:
                message.animation = 0;
                break;
            case "NONE":
            case 1:
                message.animation = 1;
                break;
            case "BREATHE":
            case 2:
                message.animation = 2;
                break;
            }
            return message;
        };

        /**
         * Creates a plain object from a Profile message. Also converts values to other types if specified.
         * @function toObject
         * @memberof Config.Profile
         * @static
         * @param {Config.Profile} message Profile
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Profile.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.type = options.enums === String ? "STATIC" : 0;
                object.breatheInterval = 0;
                object.disabledButtons = 0;
                object.animation = options.enums === String ? "NIL" : 0;
            }
            if (message.type != null && message.hasOwnProperty("type"))
                object.type = options.enums === String ? $root.Config.ProfileType[message.type] : message.type;
            if (message.breatheInterval != null && message.hasOwnProperty("breatheInterval"))
                object.breatheInterval = options.json && !isFinite(message.breatheInterval) ? String(message.breatheInterval) : message.breatheInterval;
            if (message.pstatic != null && message.hasOwnProperty("pstatic")) {
                object.pstatic = $root.Config.Profile.Static.toObject(message.pstatic, options);
                if (options.oneofs)
                    object.profileSettings = "pstatic";
            }
            if (message.preactive != null && message.hasOwnProperty("preactive")) {
                object.preactive = $root.Config.Profile.Reactive.toObject(message.preactive, options);
                if (options.oneofs)
                    object.profileSettings = "preactive";
            }
            if (message.disabledButtons != null && message.hasOwnProperty("disabledButtons"))
                object.disabledButtons = message.disabledButtons;
            if (message.animation != null && message.hasOwnProperty("animation"))
                object.animation = options.enums === String ? $root.Config.Profile.Animation[message.animation] : message.animation;
            return object;
        };

        /**
         * Converts this Profile to JSON.
         * @function toJSON
         * @memberof Config.Profile
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Profile.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        Profile.Static = (function() {

            /**
             * Properties of a Static.
             * @memberof Config.Profile
             * @interface IStatic
             * @property {Uint8Array|null} [colors] Static colors
             */

            /**
             * Constructs a new Static.
             * @memberof Config.Profile
             * @classdesc Represents a Static.
             * @implements IStatic
             * @constructor
             * @param {Config.Profile.IStatic=} [properties] Properties to set
             */
            function Static(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * Static colors.
             * @member {Uint8Array} colors
             * @memberof Config.Profile.Static
             * @instance
             */
            Static.prototype.colors = $util.newBuffer([]);

            /**
             * Creates a new Static instance using the specified properties.
             * @function create
             * @memberof Config.Profile.Static
             * @static
             * @param {Config.Profile.IStatic=} [properties] Properties to set
             * @returns {Config.Profile.Static} Static instance
             */
            Static.create = function create(properties) {
                return new Static(properties);
            };

            /**
             * Encodes the specified Static message. Does not implicitly {@link Config.Profile.Static.verify|verify} messages.
             * @function encode
             * @memberof Config.Profile.Static
             * @static
             * @param {Config.Profile.IStatic} message Static message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Static.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.colors != null && Object.hasOwnProperty.call(message, "colors"))
                    writer.uint32(/* id 1, wireType 2 =*/10).bytes(message.colors);
                return writer;
            };

            /**
             * Encodes the specified Static message, length delimited. Does not implicitly {@link Config.Profile.Static.verify|verify} messages.
             * @function encodeDelimited
             * @memberof Config.Profile.Static
             * @static
             * @param {Config.Profile.IStatic} message Static message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Static.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a Static message from the specified reader or buffer.
             * @function decode
             * @memberof Config.Profile.Static
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {Config.Profile.Static} Static
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Static.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.Config.Profile.Static();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.colors = reader.bytes();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a Static message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof Config.Profile.Static
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {Config.Profile.Static} Static
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Static.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a Static message.
             * @function verify
             * @memberof Config.Profile.Static
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            Static.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.colors != null && message.hasOwnProperty("colors"))
                    if (!(message.colors && typeof message.colors.length === "number" || $util.isString(message.colors)))
                        return "colors: buffer expected";
                return null;
            };

            /**
             * Creates a Static message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof Config.Profile.Static
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {Config.Profile.Static} Static
             */
            Static.fromObject = function fromObject(object) {
                if (object instanceof $root.Config.Profile.Static)
                    return object;
                var message = new $root.Config.Profile.Static();
                if (object.colors != null)
                    if (typeof object.colors === "string")
                        $util.base64.decode(object.colors, message.colors = $util.newBuffer($util.base64.length(object.colors)), 0);
                    else if (object.colors.length)
                        message.colors = object.colors;
                return message;
            };

            /**
             * Creates a plain object from a Static message. Also converts values to other types if specified.
             * @function toObject
             * @memberof Config.Profile.Static
             * @static
             * @param {Config.Profile.Static} message Static
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            Static.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults)
                    if (options.bytes === String)
                        object.colors = "";
                    else {
                        object.colors = [];
                        if (options.bytes !== Array)
                            object.colors = $util.newBuffer(object.colors);
                    }
                if (message.colors != null && message.hasOwnProperty("colors"))
                    object.colors = options.bytes === String ? $util.base64.encode(message.colors, 0, message.colors.length) : options.bytes === Array ? Array.prototype.slice.call(message.colors) : message.colors;
                return object;
            };

            /**
             * Converts this Static to JSON.
             * @function toJSON
             * @memberof Config.Profile.Static
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            Static.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return Static;
        })();

        Profile.Reactive = (function() {

            /**
             * Properties of a Reactive.
             * @memberof Config.Profile
             * @interface IReactive
             * @property {Uint8Array|null} [colors] Reactive colors
             * @property {Uint8Array|null} [reactColors] Reactive reactColors
             */

            /**
             * Constructs a new Reactive.
             * @memberof Config.Profile
             * @classdesc Represents a Reactive.
             * @implements IReactive
             * @constructor
             * @param {Config.Profile.IReactive=} [properties] Properties to set
             */
            function Reactive(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * Reactive colors.
             * @member {Uint8Array} colors
             * @memberof Config.Profile.Reactive
             * @instance
             */
            Reactive.prototype.colors = $util.newBuffer([]);

            /**
             * Reactive reactColors.
             * @member {Uint8Array} reactColors
             * @memberof Config.Profile.Reactive
             * @instance
             */
            Reactive.prototype.reactColors = $util.newBuffer([]);

            /**
             * Creates a new Reactive instance using the specified properties.
             * @function create
             * @memberof Config.Profile.Reactive
             * @static
             * @param {Config.Profile.IReactive=} [properties] Properties to set
             * @returns {Config.Profile.Reactive} Reactive instance
             */
            Reactive.create = function create(properties) {
                return new Reactive(properties);
            };

            /**
             * Encodes the specified Reactive message. Does not implicitly {@link Config.Profile.Reactive.verify|verify} messages.
             * @function encode
             * @memberof Config.Profile.Reactive
             * @static
             * @param {Config.Profile.IReactive} message Reactive message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Reactive.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.colors != null && Object.hasOwnProperty.call(message, "colors"))
                    writer.uint32(/* id 1, wireType 2 =*/10).bytes(message.colors);
                if (message.reactColors != null && Object.hasOwnProperty.call(message, "reactColors"))
                    writer.uint32(/* id 2, wireType 2 =*/18).bytes(message.reactColors);
                return writer;
            };

            /**
             * Encodes the specified Reactive message, length delimited. Does not implicitly {@link Config.Profile.Reactive.verify|verify} messages.
             * @function encodeDelimited
             * @memberof Config.Profile.Reactive
             * @static
             * @param {Config.Profile.IReactive} message Reactive message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Reactive.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a Reactive message from the specified reader or buffer.
             * @function decode
             * @memberof Config.Profile.Reactive
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {Config.Profile.Reactive} Reactive
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Reactive.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.Config.Profile.Reactive();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.colors = reader.bytes();
                        break;
                    case 2:
                        message.reactColors = reader.bytes();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a Reactive message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof Config.Profile.Reactive
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {Config.Profile.Reactive} Reactive
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Reactive.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a Reactive message.
             * @function verify
             * @memberof Config.Profile.Reactive
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            Reactive.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.colors != null && message.hasOwnProperty("colors"))
                    if (!(message.colors && typeof message.colors.length === "number" || $util.isString(message.colors)))
                        return "colors: buffer expected";
                if (message.reactColors != null && message.hasOwnProperty("reactColors"))
                    if (!(message.reactColors && typeof message.reactColors.length === "number" || $util.isString(message.reactColors)))
                        return "reactColors: buffer expected";
                return null;
            };

            /**
             * Creates a Reactive message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof Config.Profile.Reactive
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {Config.Profile.Reactive} Reactive
             */
            Reactive.fromObject = function fromObject(object) {
                if (object instanceof $root.Config.Profile.Reactive)
                    return object;
                var message = new $root.Config.Profile.Reactive();
                if (object.colors != null)
                    if (typeof object.colors === "string")
                        $util.base64.decode(object.colors, message.colors = $util.newBuffer($util.base64.length(object.colors)), 0);
                    else if (object.colors.length)
                        message.colors = object.colors;
                if (object.reactColors != null)
                    if (typeof object.reactColors === "string")
                        $util.base64.decode(object.reactColors, message.reactColors = $util.newBuffer($util.base64.length(object.reactColors)), 0);
                    else if (object.reactColors.length)
                        message.reactColors = object.reactColors;
                return message;
            };

            /**
             * Creates a plain object from a Reactive message. Also converts values to other types if specified.
             * @function toObject
             * @memberof Config.Profile.Reactive
             * @static
             * @param {Config.Profile.Reactive} message Reactive
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            Reactive.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults) {
                    if (options.bytes === String)
                        object.colors = "";
                    else {
                        object.colors = [];
                        if (options.bytes !== Array)
                            object.colors = $util.newBuffer(object.colors);
                    }
                    if (options.bytes === String)
                        object.reactColors = "";
                    else {
                        object.reactColors = [];
                        if (options.bytes !== Array)
                            object.reactColors = $util.newBuffer(object.reactColors);
                    }
                }
                if (message.colors != null && message.hasOwnProperty("colors"))
                    object.colors = options.bytes === String ? $util.base64.encode(message.colors, 0, message.colors.length) : options.bytes === Array ? Array.prototype.slice.call(message.colors) : message.colors;
                if (message.reactColors != null && message.hasOwnProperty("reactColors"))
                    object.reactColors = options.bytes === String ? $util.base64.encode(message.reactColors, 0, message.reactColors.length) : options.bytes === Array ? Array.prototype.slice.call(message.reactColors) : message.reactColors;
                return object;
            };

            /**
             * Converts this Reactive to JSON.
             * @function toJSON
             * @memberof Config.Profile.Reactive
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            Reactive.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return Reactive;
        })();

        /**
         * Animation enum.
         * @name Config.Profile.Animation
         * @enum {number}
         * @property {number} NIL=0 NIL value
         * @property {number} NONE=1 NONE value
         * @property {number} BREATHE=2 BREATHE value
         */
        Profile.Animation = (function() {
            var valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "NIL"] = 0;
            values[valuesById[1] = "NONE"] = 1;
            values[valuesById[2] = "BREATHE"] = 2;
            return values;
        })();

        return Profile;
    })();

    /**
     * ControllerType enum.
     * @name Config.ControllerType
     * @enum {number}
     * @property {number} GENERIC=0 GENERIC value
     * @property {number} SMASH_BOX=1 SMASH_BOX value
     * @property {number} HIT_BOX=2 HIT_BOX value
     * @property {number} CROSSUP=3 CROSSUP value
     * @property {number} BBG_NOSTALGIA=4 BBG_NOSTALGIA value
     */
    Config.ControllerType = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "GENERIC"] = 0;
        values[valuesById[1] = "SMASH_BOX"] = 1;
        values[valuesById[2] = "HIT_BOX"] = 2;
        values[valuesById[3] = "CROSSUP"] = 3;
        values[valuesById[4] = "BBG_NOSTALGIA"] = 4;
        return values;
    })();

    /**
     * ControllerVariant enum.
     * @name Config.ControllerVariant
     * @enum {number}
     * @property {number} OTHER=0 OTHER value
     * @property {number} FIGHTSTICK=1 FIGHTSTICK value
     * @property {number} STICKLESS=2 STICKLESS value
     * @property {number} FIGHTSTICK_EXD=3 FIGHTSTICK_EXD value
     */
    Config.ControllerVariant = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "OTHER"] = 0;
        values[valuesById[1] = "FIGHTSTICK"] = 1;
        values[valuesById[2] = "STICKLESS"] = 2;
        values[valuesById[3] = "FIGHTSTICK_EXD"] = 3;
        return values;
    })();

    return Config;
})();

module.exports = $root;
